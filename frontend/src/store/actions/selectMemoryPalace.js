const selectMemoryPalace = (context, id) => {
    context.commit('SELECT_MEMORY_PALACE', id)
}

export default selectMemoryPalace
