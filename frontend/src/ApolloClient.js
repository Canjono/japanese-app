import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

// Local ip
const ip = '127.0.0.1'

// Home network ip
// const ip = '192.168.1.233'

const httpLink = new HttpLink({
    uri: `http://${ip}:3000/graphql`
})

// Create the apollo client
const apolloClient = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
    connectToDevTools: true
})

export default apolloClient
