import { GraphQLObjectType, GraphQLString, GraphQLNonNull } from 'graphql'

const memoryPalaceRoomType = new GraphQLObjectType({
    name: 'MemoryPalaceRoom',
    description: 'A memory palace room',
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the memory palace room'
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The name of the memory palace room'
        },
        createdAt: {
            type: GraphQLString,
            description: 'Date when the memory palace room was created'
        },
        updatedAt: {
            type: GraphQLString,
            description: 'Date when the memory palace room was last updated'
        }
    }
})

export default memoryPalaceRoomType
