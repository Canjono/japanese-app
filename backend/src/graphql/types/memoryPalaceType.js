import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList
} from 'graphql'
import memoryPalaceRoom from './memoryPalaceRoomType'

const memoryPalaceType = new GraphQLObjectType({
    name: 'MemoryPalace',
    description: 'A memory palace',
    fields: {
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the memory palace'
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The name of the memory palace'
        },
        createdAt: {
            type: GraphQLString,
            description: 'Date when the memory palace was created'
        },
        updatedAt: {
            type: GraphQLString,
            description: 'Date when the memory palace was last updated'
        },
        rooms: {
            type: new GraphQLList(memoryPalaceRoom),
            description: 'Rooms associated with this memory palace'
        }
    }
})

export default memoryPalaceType
