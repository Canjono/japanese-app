import { GraphQLList, GraphQLInt } from 'graphql'
import MemoryPalace from '../../database/models/memoryPalace'
import MemoryPalaceRoom from '../../database/models/memoryPalaceRoom'
import memoryPalaceType from '../types/memoryPalaceType'

const getMemoryPalaces = {
    type: new GraphQLList(memoryPalaceType),
    args: {
        limit: {
            type: GraphQLInt,
            description: 'Limits the number of results returned'
        },
        offset: {
            type: GraphQLInt
        }
    },
    resolve: (obj, args) => {
        console.log(`getMemoryPalaces request args: ${JSON.stringify(args)}`)
        const offset = args.offset || 0
        const limit = args.limit || 100

        return MemoryPalace.findAll({
            include: [
                {
                    model: MemoryPalaceRoom,
                    as: 'rooms',
                    required: true
                }
            ],
            where: { enabled: true },
            offset,
            limit,
            attributes: { exclude: ['enabled'] }
        })
            .then(palaces => {
                return palaces
            })
            .catch(error =>
                console.log(`MemoryPalace.findAll error: ${error}`)
            )
    }
}

export default getMemoryPalaces
