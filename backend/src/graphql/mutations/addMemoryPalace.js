import { GraphQLNonNull, GraphQLString, GraphQLList } from 'graphql'
import MemoryPalace from '../../database/models/memoryPalace'
import MemoryPalaceRoom from '../../database/models/memoryPalaceRoom'
import memoryPalaceType from '../types/memoryPalaceType'
import uuid from 'uuid'

const addMemoryPalace = {
    type: memoryPalaceType,
    args: {
        name: {
            description: 'Name of the memory palace',
            type: new GraphQLNonNull(GraphQLString)
        },
        rooms: {
            description: 'Name of the rooms',
            type: new GraphQLList(GraphQLString)
        }
    },
    resolve: (obj, args) => {
        console.log(`addMemoryPalace request args: ${JSON.stringify(args)}`)
        let rooms = []

        args.rooms.forEach(room => {
            rooms.push({
                id: uuid(),
                name: room,
                createdAt: Date.now(),
                updatedAt: Date.now()
            })
        })

        return MemoryPalace.create(
            {
                id: uuid(),
                name: args.name,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                rooms: rooms
            },
            {
                include: [{ model: MemoryPalaceRoom, as: 'rooms' }]
            }
        ).then(palace => {
            return palace
        })
    }
}

export default addMemoryPalace
