import Sequelize from 'sequelize'
import db from '../db'
import MemoryPalaceRoom from './memoryPalaceRoom'

const memoryPalace = db.define('memory_palace', {
    id: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    createdAt: {
        type: Sequelize.DATE
    },
    updatedAt: {
        type: Sequelize.DATE
    },
    enabled: {
        type: Sequelize.BOOLEAN
    }
})

memoryPalace.belongsToMany(MemoryPalaceRoom, {
    through: 'memory_palace_to_room',
    as: 'rooms'
})

export default memoryPalace
